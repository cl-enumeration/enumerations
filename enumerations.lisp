;;;; -*- Mode: Lisp -*-

;;;; enumerations.lisp --
;;;; The main definitions for the CL Enumerations API.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; Main definitions.

(define-condition no-such-element (error)
  ((enum :reader no-such-element-enumeration :initarg :enumeration))
  (:documentation
   "The condition signalled  when no reasonable element is available.

Many methods (e.g., NEXT) signal this condition if no element is
available in and ENUMERATION of some (enumerable) object.")
  (:report (lambda (cnd stream)
	     (format stream
		     "Enumeration ~S has no more elements to yield."
		     (no-such-element-enumeration cnd)))))


(defclass enumeration ()
  ((enumerated-object :reader enumeration-object
		      :initarg :object)
   (cursor :accessor enumeration-cursor)
   )
  (:documentation "The CL Extensions Enumeration Class.

The 'root' of all the different enumeration classes.")
  (:default-initargs :object nil))


(defgeneric enumerationp (x)
  (:method ((x t)) nil)
  (:method ((x enumeration)) t))


(defmethod print-object ((e enumeration) stream)
  (print-unreadable-object (e stream :type nil :identity t)
      (format stream "~@[~*Empty ~]~A enumeration"
	      (not (has-next-p e))
	      (type-of (enumeration-object e))
	      )))


;;;;===========================================================================
;;;; Protocol.

(defgeneric enumerate (enumerable-item
		       &key start end
		       &allow-other-keys)
  (:documentation
   "Creates a (specialized) ENUMERATION object.
If applicable START and END delimit the range of the actual enumeration.
The generic function ENUMERATE is the main entry point in the
ENUMERATIONS machinery. It takes a CL object (ENUMERABLE-ITEM) and
some additional parameters, which may or may not be used depending on
the type of the object to be enumerated.


Arguments and Values:

ENUMERABLE-ITEM : an Common Lisp object
START : an object (usually a non-negative integer)
END : an object or NIL
result : an ENUMERATION instance


Exceptional Situations:

ENUMERATE calls MAKE-INSTANCE on the appropriate ENUMERATION
sub-class. The instance initialization machinery may signal various
errors. See the documentation for each ENUMERATION sub-class for
details.
"))


(defgeneric has-more-elements-p (x)
  (:method ((x enumeration)) nil)
  #|
  (:documentation
   "Checks whether the enumeration has more elements. Obsolete.")|#)


(defgeneric has-next-p (x)
  (:method ((e enumeration)) nil)
  (:documentation
   "The generic function HAS-NEXT-P checks whether the enumeration
enumeration has another element that follows the current one in the
traversal order. If so it returns a non-NIL result, otherwise, result
is NIL.

Arguments and Values:

X : an ENUMERATION instance
result : a T.

Examples:

cl-prompt> (defvar ve (enumerate (vector 1 2 3)))
VE
 
cl-prompt> (has-next-p ve)
T
 
cl-prompt> (loop repeat 3 do (print (next ve)))
1
2
3
NIL
 
cl-prompt> (has-next-p ve)
NIL

Exceptional Situations:

HAS-NEXT-P signals an error (i.e., NO-APPLICABLE-METHOD is run) if X is not
an ENUMERATION instance.
"
))


(defgeneric next (e &optional default)
  (:documentation
   "The generic function NEXT returns the \"next\" object in the
enumeration enumeration if there is one, as determined by calling
HAS-NEXT-P. If HAS-NEXT-P returns NIL and DEFAULT is supplied, then
DEFAULT is returned. Otherwise the condition NO-SUCH-ELEMENT is
signaled.

Arguments and Values:

E : an ENUMERATION instance
DEFAULT : a T
result : a T.


Exceptional Situations:

NEXT signals the NO-SUCH-ELEMENT condition when there are no more
elements in the enumeration and no default was supplied."))


(defgeneric current (enum &optional errorp default)
  (:method ((x enumeration) &optional (errorp t) default)
   (declare (ignore errorp))
   (cerror "Do you want to return the default value specified ~S?"
           "No specific cursor defined for enumeration ~*~S." default x)
   default)
  (:documentation
   "Returns the \"current\" element in the enumeration ENUM.

Each ENUMERATION instance maintains a reference to its \"current\"
element (if within \"range\"). Given an ENUMERATION instance
enumeration, the generic function CURRENT returns the object in such
reference.

If ERRORP is non-NIL and no \"current\" element is available, then
CURRENT signals an error: either NO-SUCH-ELEMENT or a continuable
error. If ERRORP is NIL and no \"current\" element is available then
DEFAULT is returned.

Arguments and Values:

ENUM : an ENUMERATION instance
ERRORP : a generalized BOOLEAN
DEFAULT : a T
result : a T.

Exceptional Situations:

CURRENT may signal a continuable error or NO-SUCH-ELEMENT. See above
for an explanation."
   ))


(defgeneric reset (enum)
  (:documentation
   "Resets the enumeration ENUM internal state to its \"initial\" element.

The \"initial\" element of the enumeration ENUM obviously depends on
the actual enumerate object.

Arguments and Values:

ENUM : an ENUMERATION instance
result : a T.


Examples:

cl-prompt> (defvar *se* (enumerate \"foobar\" :start 2))
*SE*
 
cl-prompt> (loop repeat 3 do (print (next *se*)))
#\o
#\b
#\a
NIL
 
cl-prompt> (current *se*)
#\r
 
cl-prompt> (reset *se*)
2
 
cl-prompt> (current *se*)
#\o
 
cl-prompt> (loop repeat 3 do (print (next *se*)))
#\o
#\b
#\a
NIL
"))


(defgeneric element-type (x)
  (:method ((x enumeration)) t)
  (:documentation
   "Returns the type of the elements in the underlying data structure."))


;;;;===========================================================================
;;;; Implementation.
;;;; Bare bone general implementation of some methods.


(defmethod next ((x enumeration) &optional default)
  (declare (ignore default))
  (error "Method NEXT unimplemented for enumeration ~S." x))


(defmethod next :around ((x enumeration)
			 &optional
			 (default nil default-supplied-p))
  (cond ((has-next-p x) (call-next-method))
	(default-supplied-p default)
	(t (error 'no-such-element :enumeration x))))


;;; Enumerating an enumeration is an  identity operation.
;;; Notes:
;;; Maybe we could copy the enumeration and provide ways to "modify" it.

(defmethod enumerate ((x enumeration)
		       &key start end
		       &allow-other-keys)
  (declare (ignore start end))
  x)


;;;;---------------------------------------------------------------------------
;;;; Bi-directional enumerations.

(defclass bi-directional-enumeration (enumeration)
  ()
  (:documentation "The Bi-directional Enumeration Class.
Enumerations that can be traversed back and forth."))


(defgeneric bi-directional-enumeration-p (x)
  (:method ((e t)) nil)
  (:method ((e bi-directional-enumeration)) t))


;;;;---------------------------------------------------------------------------
;;;; Protocol for bi-directional enumerations.

(defgeneric previous (x &optional default)
  (:method ((x enumeration) &optional default)
   (declare (ignore default))
   (error "~S is not a bi-directional enumeration." x)))


(defgeneric has-previous-p (x)
  (:method ((x enumeration))
   (error "~S is not a bi-directional enumeration." x)))


(defmethod previous :around ((x enumeration)
                             &optional
                             (default nil default-supplied-p))
  (cond ((has-previous-p x) (call-next-method))
	(default-supplied-p default)
	(t (error 'no-such-element :enumeration x))))


;;;;===========================================================================
;;;; Bounded Enumerations.

(defclass bounded-enumeration (enumeration)
  ((start :accessor enumeration-start :initarg :start)
   (end :accessor enumeration-end :initarg :end)
   )
  (:default-initargs :start 0 :end nil))


(defgeneric bounded-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x bounded-enumeration)) nil))


;;;;===========================================================================
;;;; Functional Enumerations.
;;;; I.e. enumerations that depend on an "implicit" data structure,
;;;; e.g. numbers.

(defclass functional-enumeration (enumeration)
  ()
  )


(defgeneric functional-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x functional-enumeration)) t))


;;;; end of file -- enumerations.lisp --
