;;;; -*- Mode: Lisp -*-

;;;; sequence-enumeration.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; Sequence Enumeration.

(defclass sequence-enumeration (bounded-enumeration)
  ()
  (:documentation "The Sequence Enumeration Class."))

(defgeneric sequence-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x sequence-enumeration)) t))

;;;; end of file -- sequence-enumerations.lisp --
