;;; -*- Mode: Lisp -*-

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")


;;; Stream enumerations.

(defparameter *buffered-enumeration-buffer-length* 1024)


(defclass buffered-enumeration ()
  ((buffer :reader buffered-enumeration-buffer
	   :initarg :buffer))
  (:default-initargs :buffer (make-array *buffered-enumeration-buffer-length*
					 :fill-pointer 0))
  )


(defclass line-enumeration () ())

(defclass character-enumeration () ())

(defclass sexpr-enumeration () ())

(defclass byte-enumeration () ())


(defclass stream-enumeration (enumeration)
  ((enumerated-object :reader stream-enumeration-stream
		      :type stream)
   (eof-error-p :reader eof-error-p
		:initarg :eof-error-p)
   (eof-value :reader eof-value
	      :initarg :eof-value)
   )
  (:default-initargs :object *standard-input* :eof-error-p t :eof-value nil)
  (:documentation "The Stream Enumeration Class."))




(defclass character-stream-enumeration (stream-enumeration
					character-enumeration)
  ())



(defclass line-stream-enumeration (stream-enumeration
				   line-enumeration)
  ())


(defclass binary-stream-enumeration (stream-enumeration
				     byte-enumeration)
  ())


(defclass buffered-line-stream-enumeration (stream-enumeration
					    line-enumeration
					    buffered-enumeration)
  ()
  (:default-initargs :buffer (make-array *buffered-enumeration-buffer-length*
					 :element-type 'character
					 :fill-pointer 0)))


(defclass sexpr-stream-enumeration (stream-enumeration
				    sexpr-enumeration)
  ())


(defclass sequence-stream-enumeration (stream-enumeration
				       buffered-enumeration)
  ((element-type :reader element-type
		 :initarg :element-type))
  (:default-initargs :element-type t))


(defmethod enumerate ((s stream)
		      &key (start 0) end
		      (element-type (stream-element-type s))
		      (buffered nil)
		      (buffer nil)
		      &allow-other-keys)
  (declare (ignore start end))
  (cond ((subtypep element-type 'cons)
	 (make-instance 'sexpr-stream-enumeration :object s))
	((subtypep element-type 'character)
	 (make-instance 'sexpr-stream-enumeration :object s))
	((or (subtypep element-type '(unsigned-byte *))
	     (subtypep element-type '(signed-byte *)))
	 (make-instance 'binary-stream-enumeration :object s))
	((or (subtypep element-type 'string)
	     (subtypep element-type 'vector)) ; For non simple-arrays.
	 (if buffered
	     (if buffer
		 (make-instance 'buffered-line-stream-enumeration
				:buffer buffer
				:object s)
		 (make-instance 'buffered-line-stream-enumeration
				:object s))
	     (make-instance 'line-stream-enumeration
			    :object s))
	 )))



(defmethod initialize-instance :after ((se stream-enumeration) &key)
  (unless (open-stream-p (stream-enumeration-stream se))
    (error "Trying to create a stream enumeration on a closed stream ~S."
	    (stream-enumeration-stream se)))
  )


(defmethod initialize-instance :after ((se sequence-stream-enumeration)
				       &key (element-type t))
  (let* ((beb (buffered-enumeration-buffer se))
	 (be-type (array-element-type beb)))
    (unless (subtypep be-type element-type)
      (setf (slot-value se 'buffer)
	    (make-array (length beb)
			:fill-pointer (fill-pointer beb)
			:element-type element-type)))))


(defgeneric stream-end-p (stream))

(defmethod stream-end-p ((stream stream))
  (flet ((character-stream-end-p (stream)
	   (cond ((interactive-stream-p stream)
		  (when (listen stream)
		    (null (peek-char nil stream nil nil)))
		  )
		 (t (null (peek-char nil stream nil nil))))
	   )
	 (binary-stream-end-p (stream)
	   (when (interactive-stream-p stream)
	     (error "Unsupported interactive binary stream."))
	   (listen stream)
	   )
	 (generic-stream-end-p (stream)
	   (when (interactive-stream-p stream)
	     (error "Unsupported interactive stream with ~S elements."
		    (stream-element-type stream)))
	   (listen stream)
	   )
	 )
    (cond ((and (open-stream-p stream)
		(input-stream-p stream))
	   (let ((element-type (stream-element-type stream)))
	     (cond ((subtypep element-type 'character)
		    (character-stream-end-p stream))
		   ((or (subtypep element-type '(unsigned-byte *))
			(subtypep element-type '(signed-byte *)))
		    (binary-stream-end-p stream))
		   (t (generic-stream-end-p stream)))
	     ))
	  (t t))))


;; #+file-stream-class-defined
(defmethod stream-end-p ((stream file-stream))
  (flet ((character-stream-end-p (stream)
	   (null (peek-char nil stream nil nil)))

	 (binary-stream-end-p (stream)
	   (listen stream))
	 
	 (generic-stream-end-p (stream)
	   (listen stream))
	 )
    (cond ((and (open-stream-p stream)
		(input-stream-p stream))
	   (let ((element-type (stream-element-type stream)))
	     (cond ((subtypep element-type 'character)
		    (character-stream-end-p stream))
		   ((or (subtypep element-type '(unsigned-byte *))
			(subtypep element-type '(signed-byte *)))
		    (binary-stream-end-p stream))
		   (t (generic-stream-end-p stream)))
	     ))
	  (t t))))



(defmethod has-more-elements-p ((se stream-enumeration))
  (and (open-stream-p (stream-enumeration-stream se))
       (not (stream-end-p (stream-enumeration-stream se)))))






(defmethod next ((se character-stream-enumeration)
		 &optional (default (code-char 0)))
  (declare (ignore default))
  (read-char (stream-enumeration-stream se)  (eof-error-p se) (eof-value se)))


(defmethod next ((se binary-stream-enumeration)
		 &optional (default 0))
  (declare (ignore default))
  (read-byte (stream-enumeration-stream se)  (eof-error-p se) (eof-value se)))


(defmethod next ((se sexpr-stream-enumeration)
		 &optional (default ()))
  (declare (ignore default))
  (read (stream-enumeration-stream se) (eof-error-p se) (eof-value se)))


(defmethod next ((se line-stream-enumeration)
		 &optional (default (code-char 0)))
  (declare (ignore default))
  (read-line (stream-enumeration-stream se) (eof-error-p se) (eof-value se)))


(defmethod next ((se buffered-line-stream-enumeration)
		 &optional (default ""))
  (declare (ignore default))
  (let ((buffer (buffered-enumeration-buffer se))
	(s (stream-enumeration-stream se))
	(eof-value (eof-value se))
	(eof-error-p (eof-error-p se))
	)
    (setf (fill-pointer buffer) 0)
    (loop for c of-type (or null character)
	      = (read-char s eof-error-p eof-value)
	  while c
	    if (char= c #\Newline)
	      return buffer
	    else
	      do (vector-push-extend c buffer)
            end
	  finally (return buffer)
	  )))
  


;;; end of file -- stream-enumerations.lisp --
