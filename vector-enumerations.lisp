;;;; -*- Mode: Lisp -*-

;;;; vector-enumeration.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; Vector Enumeration.

(defclass vector-enumeration (sequence-enumeration bi-directional-enumeration)
  ((cursor :type fixnum)))


(defgeneric vector-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x vector-enumeration)) t))


(defmethod initialize-instance :after ((x vector-enumeration) &key)
  (setf (enumeration-cursor x) (enumeration-start x))
  (unless (enumeration-end x)
    (setf (enumeration-end x) (length (enumeration-object x)))))


(defmethod enumerate ((v vector) &key (start 0) end &allow-other-keys)
  "Calling ENUMERATE on a VECTOR returns a VECTOR-ENUMERATION instance.

START must be an integer between 0 and (length V). end must be an
integer between 0 and (length V) or NIL. The usual CL semantics
regarding sequence traversals applies."
  (make-instance 'vector-enumeration :object v :start start :end end))


(defmethod has-more-elements-p ((x vector-enumeration))
  (< (enumeration-cursor x) (enumeration-end x)))


(defmethod has-next-p ((x vector-enumeration))
  (< (enumeration-cursor x) (enumeration-end x)))


(defmethod next ((x vector-enumeration) &optional default)
  (declare (ignore default))
  (prog1 (aref (enumeration-object x) (enumeration-cursor x))
    (incf (enumeration-cursor x))))


(defmethod has-previous-p ((x vector-enumeration))
  (plusp (enumeration-cursor x)))


(defmethod previous ((x vector-enumeration) &optional default)
  (declare (ignore default))
  (decf (enumeration-cursor x))
  (aref (enumeration-object x) (enumeration-cursor x)))


(defmethod current ((x vector-enumeration) &optional (errorp t) (default nil))
  (cond ((and errorp (not (has-more-elements-p x)))
         (error 'no-such-element :enumeration x))
        ((and (not errorp) (not (has-next-p x)))
         default)
        (t (aref (enumeration-object x) (enumeration-cursor x)))))


(defmethod reset ((x vector-enumeration))
  (setf (enumeration-cursor x) (enumeration-start x)))


;;; end of file -- vector-enumerations.lisp --
