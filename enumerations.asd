;;; -*- Mode: Lisp -*-

;;;; enumerations.asd --
;;;;
;;;; See file COPYING for copyright and licensing information.

(asdf:defsystem :enumerations
  :description "The CL-ENUMERATIONS project contains a Common Lisp
Java-like enumeration/iteration library and protocol. Most basic
Common Lisp types are handled.

The project page can be found at
http://common-lisp.net/project/cl-enumeration."

  :author "Marco Antoniotti <mantoniotti (you-know-the-drill) common-lisp.net>"

  :license "BSD like"

  :components ((:file "enumerations-pkg")
               (:file "enumerations" :depends-on ("enumerations-pkg"))
               (:file "sequence-enumeration" :depends-on ("enumerations-pkg"))
               (:file "list-enumerations" :depends-on ("sequence-enumeration"))
               (:file "vector-enumerations" :depends-on ("sequence-enumeration"))
               (:file "string-enumerations" :depends-on ("vector-enumerations"))
               (:file "hash-table-enumerations" :depends-on ("enumerations"))
               (:file "array-enumerations" :depends-on ("enumerations"))
               (:file "number-enumerations" :depends-on ("enumerations"))
               (:file "foreach" :depends-on ("enumerations"))
	       #+:iterate (:file "iterate-extension"
				 :depends-on ("enumerations"))
               ))

(pushnew :cl-enumerations *features*)

;;; end of file -- enumerations.asd --
