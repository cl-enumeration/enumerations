;;;; -*- Mode: Lisp -*-

;;;; list-enumerations.lisp --
;;;; Eunmerations for lists.
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CL.EXTENSIONS.DACF.ENUMERATIONS")

;;;;===========================================================================
;;;; List Enumeration.

(defclass list-enumeration (sequence-enumeration)
  ((end-cons :accessor end-cons))
  (:documentation "The List Enumeration Class."))

(defgeneric list-enumeration-p (x)
  (:method ((x t)) nil)
  (:method ((x list-enumeration)) t))


(defmethod initialize-instance :after ((x list-enumeration) &key)
  (with-slots (enumerated-object)
    x
    (setf (enumeration-cursor x)
	  (nthcdr (enumeration-start x) enumerated-object))

    (if (and (enumeration-end x)
	     (< (enumeration-end x) (length enumerated-object)))
	(setf (end-cons x)
	      (nthcdr (enumeration-end x) enumerated-object))
	(setf (end-cons x)
	      (last enumerated-object))
	)))


(defmethod enumerate ((l list) &key (start 0) end &allow-other-keys)
"Calling ENUMERATE on a LIST returns a LIST-ENUMERATION instance.
L should be a proper list. The behavior of ENUMERATE when passed a non
proper list is undefined.

START must be an integer between 0 and (length l). END must be an
integer between 0 and (length L) or NIL. The usual CL semantics
regarding sequence traversals applies."
  (make-instance 'list-enumeration :object l :start start :end end))


(defmethod has-more-elements-p ((x list-enumeration))
  (not (null (enumeration-cursor x))))


(defmethod has-next-p ((x list-enumeration))
  (not (null (enumeration-cursor x))))


(defmethod next ((x list-enumeration) &optional default)
  (declare (ignore default))
  (with-slots (cursor)
     x
     (prog1 (first cursor)
       (if (eq cursor (end-cons x))
	   (setf cursor nil)
	   (setf cursor (rest cursor))))))

(defmethod reset ((x list-enumeration))
  (setf (enumeration-cursor x)
	(nthcdr (enumeration-start x) (enumeration-object x))))


(defmethod current ((x list-enumeration) &optional (errorp t) (default nil))
  (cond ((and errorp (not (has-next-p x)))
         (error 'no-such-element :enumeration x))
        ((and (not errorp) (not (has-next-p x)))
         default)
        (t (first (enumeration-cursor x)))))

;;;; end of file -- list-enumerations.lisp --
